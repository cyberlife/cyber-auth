const PeerId = require('peer-id')
const PeerInfo = require('peer-info')
const Node = require('../libp2p-bundle')
const pull = require('pull-stream')
const Pushable = require('pull-pushable')
const async = require('async')
const p = Pushable()

var protocolName = "/cyberlife"

function dialNode(personalNode, node, dataArray) {

  if (dataArray.constructor !== Array || dataArray.length == 0) {

    return -1

  }

  personalNode.dialProtocol(node, protocolName, (err, conn) => {

    if (err) {
      return undefined
    }

    pull(pull.values(dataArray), conn)

  })

}

module.exports = {

  dialNode

}
