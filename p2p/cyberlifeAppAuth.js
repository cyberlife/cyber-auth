var network = require('./network')

async function checkAgentFunction(jobId, agentName, caller, host, typesArr, paramsArr) {

  try {

    var baseErr = jobId.toString() + " " + agentName.toString() + " -- "
    var errLog = baseErr

    var sanitizedCaller = await checkNode(caller)
    var sanitizedHost = await checkHost(host)

    var sanitizedParams = await checkParamsTypes(typesArr, paramsArr)

    if (sanitizedCaller == false)
      errLog += "The caller's node is not correctly setup \n"

    if (sanitizedHost == false)
      errLog += "The host's node is not correctly setup \n"

    if (sanitizedParams != "Ok") {

      errLog += sanitizedParams + "\n"

    }

    if (errLog != baseErr) {

      await network.dialNode(host, caller, [errLog])

      return false

    } else {

      return true

    }

  } catch (err) {

    errLog += err + '\n'

    await network.dialNode(host, caller, [errLog])

    return false

  }

}

async function checkHost(host) {

  return (

    host.peerInfo.id.toB58String().length == 46

  )

}

async function checkNode(node) {

  return (

    node != undefined &&
    typeof node == "object" &&
    node.id.toB58String().length == 46

  )

}

async function checkParamsTypes(typesArr, paramsArr) {

  var wrongParamTypes = "Did not correctly specify parameter types"

  if (typesArr.length == 0) return "Ok"

  if (typesArr.length != paramsArr.length)
    return wrongParamTypes

  var wrongTypes = []

  for (var i = 0; i < typesArr.length; i++) {

    if (typesArr[i] != typeof paramsArr[i])
      wrongTypes.push()

  }

  if (wrongTypes.length == 0)
    return "Ok"

  return "The following params do not have the correct types: " + JSON.stringify(wrongTypes)

}

module.exports = {

  checkHost,
  checkAgentFunction,
  checkNode,
  checkParamsTypes

}
