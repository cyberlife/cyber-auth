const IPFS = require('ipfs-api');
const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });

const async = require('async')

async function postDatatoIPFS(data, callback) {

  await ipfs.add(data, (err, ipfsHash) => {

      if (err != undefined) return callback(err, null)

      return callback(null, ipfsHash)

  })

}

async function getIPFSData(ipfsAddress, callback) {

  await ipfs.get(ipfsAddress, (err, data) => {

      if (err != undefined) {return callback(err, null)}

      return callback(null, data)

  })

}

module.exports = {

  postDatatoIPFS,
  getIPFSData

}
